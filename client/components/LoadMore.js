import React, { Component } from 'react';
import actions from  '../actions';

class LoadMore extends Component {

  render() {
    return (
      <div className="load-more">
        <button onClick={this.handleClick.bind(this)}>Load more</button>
      </div>
    )
  }

  handleClick() {
    this.props.dispatch({type: actions.LOAD_UPDATES, limit: 20, offset: this.props.updatesCount});
  }
}

export default LoadMore;
