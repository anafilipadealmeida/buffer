import React, { Component } from 'react';
import Update from  './Update';
import LoadMore from './LoadMore';

class UpdateList extends Component {

  render() {
    if (!this.props.updates || !this.props.updates.length) {
      return <p className="no-updates">You have no updates</p>
    }
    return (
      <div className="update-list">
        {this.props.updates.map((update, idx) => (
          <Update
            {...update}
            key={idx}
          />
        ))}
        {!this.props.updatesLoadedAll &&
        <LoadMore
          updatesCount={this.props.updates.length}
          dispatch={this.props.dispatch}
        />
        }
      </div>
    )
    return
  }
}

export default UpdateList;
