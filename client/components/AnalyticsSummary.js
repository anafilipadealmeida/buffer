import React, { Component } from 'react';

class AnalyticsSummary extends Component {

  calculateAnalytics() {

    let stats = {
      retweets: 0,
      favorites: 0,
      clicks: 0
    };

    this.props.updates && this.props.updates.map((update,index) => {
      for (let type in update.statistics) {
        stats[type] += parseInt(update.statistics[type], 10);
      }
    });

    return stats;
  }


  render() {
    return (
      <div className="analytics-summary">
        <p className="analytics-summary-number analytics-summary-post">{this.props.updates && this.props.updates.length || 0}</p>
        <p className="analytics-summary-type">Posts</p>
        <p className="analytics-summary-number analytics-summary-retweet"> {this.calculateAnalytics().retweets}</p>
        <p className="analytics-summary-type">Retweets</p>
        <p className="analytics-summary-number analytics-summary-favorite"> {this.calculateAnalytics().favorites}</p>
        <p className="analytics-summary-type">Favorites</p>
        <p className="analytics-summary-number analytics-summary-click"> {this.calculateAnalytics().clicks}</p>
        <p className="analytics-summary-type">Clicks</p>
      </div>
    )
    return
  }
}

export default AnalyticsSummary;
