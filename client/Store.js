import Api from './Api'
import actions from './actions'

window.Api = Api;

class Store {

  constructor(initialState = {}) {
    this.state = initialState;
    this.onChangeCallbacks = [];
  }

  getState(key) {
    if (key) {
      return this.state[key];
    }
    return this.state
  }

  setState(key, value) {
    this.state[key] = value;
    this.emitChange();
  }

  onChange(callback) {
    this.onChangeCallbacks.push(callback);
  }

  emitChange() {
    this.onChangeCallbacks.forEach(callback => {
      callback();
    })
  }

  /**
   * dispatch
   * Used to trigger an event throughout the application with a given action.
   * ex.
   *   store.dispatch({ action: actions.LOAD_UPDATES })
   */
  dispatch(action) {
    switch (action.type) {
      case actions.LOAD_UPDATES:
        let params = {
          offset: action.offset || "",
          limit: action.limit || ""
        };
        Api.get('getUpdates', params)
          .then(updates => {
            let newUpdates = this.getState('updates') && this.getState('updates').concat(updates) || updates;
            this.setState('updates', newUpdates);
            this.dispatch({type: actions.COUNT_UPDATES});
          })
          .catch(err => console.error('err?', err))
        break;
      case actions.COUNT_UPDATES:
        Api.get('getUpdatesCount')
          .then(count => {
            let updatesLoadedAll = this.getState('updates') && (this.getState('updates').length >= count.count);
            this.setState('updatesLoadedAll', updatesLoadedAll);
          })
          .catch(err => console.error('err?', err))
        break;
    }
  }
}

export default Store;
