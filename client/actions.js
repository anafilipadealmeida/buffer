const actions = {
  LOAD_UPDATES: 'LOAD_UPDATES',
  COUNT_UPDATES: 'COUNT_UPDATES'
};

export default actions;
